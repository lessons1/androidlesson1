package com.example.p030activityresult;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import javax.xml.transform.Result;

public class ColorActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnBlue;
    Button btnGreen;
    Button btnRed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color);

        btnBlue = (Button) findViewById(R.id.btnBlue);
        btnGreen = (Button) findViewById(R.id.btnGreen);
        btnRed = (Button) findViewById(R.id.btnRed);

        btnBlue.setOnClickListener(this);
        btnGreen.setOnClickListener(this);
        btnRed.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()){
            case R.id.btnBlue :
                intent.putExtra("color", Color.BLUE);
                break;
            case R.id.btnRed :
                intent.putExtra("color", Color.RED);
                break;
            case R.id.btnGreen :
                intent.putExtra("color", Color.GREEN);
                break;
        }
        setResult(RESULT_OK,intent);
        finish();
    }
}
